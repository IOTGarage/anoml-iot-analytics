# AnoML.py
# StatsTS Model converts
def StatsTS(DF, TS, fx):
    ar = []
    for i in range (DF.shape[0] - TS):
        m = DF.iloc[i:i+TS]
        m=fx(m.to_numpy())
        ar.append(m)
    for y in range(TS):
        ar.append(m)
    return np.array(ar)
def AnoMLTest():
    print ("AnoML verion 0.01")
def Convert2TimeSeries(DF, DPs, TS, AE):
    from time import time
    import numpy as np
    Series, Value = [], []
    ST=time()
    DF[DPs] = DF[DPs].fillna(0)
    for i in range(len(DF) - TS):
        Series.append(DF[DPs].iloc[i:i+TS])
        if (AE):
            Value.append(DF[DPs].iloc[i:i+TS])
        else:
            Value.append(DF[DPs].iloc[i + TS])
    #Last values (len - TS) will be static
    for j in range(TS):
        Series.append(Series[-1])
        Value.append(Value[-1])
    Series = np.array(Series)
    Value = np.array(Value)
    ET=time()
    TT = 1000*(ET-ST)
    print ("Time consumed in TS conversion", TT, "for ", DF[DPs].shape)
    del time
    del np
    return Series, Value
#Extract Data from all files
def MultiDeviceDataExtractor(FileList, DeviceTypes, SensorColumn):
  DF={}
  import pandas as pd
  for HeaderNumber in range(len(DeviceTypes)):
      DataPoints = DeviceTypes[HeaderNumber].replace("\n", "").split(",")
      Device = DataPoints[SensorColumn]
      Readings = DataPoints[9:-1]
      Location = DataPoints[-1:]
      columns = [x for x in range(len(DataPoints))]
      temp_df = []
      for ND in FileList:
          tdf=pd.read_csv(ND, header=None, usecols=columns, names=DataPoints)
          temp_df.append(tdf)
      temp_df = pd.concat(temp_df, axis=0, ignore_index=True)
      cond=temp_df[DataPoints[7]]==DataPoints[7]
      DF[Device] = temp_df[cond].copy()
      DF[Device] = DF[Device].reset_index()
      DF[Device]["datetime"] = pd.to_datetime(DF[Device]["datetime"], format="%Y%m%d%H%M", errors="ignore")
      del temp_df
      del cond
      # DF[Device].office_status = [office_status_codes[item] for item in DF[Device].office_status]
      # DF[Device].day = [day_codes[item] for item in DF[Device].day]
      for R in Readings:
          DF[Device][R] = pd.to_numeric(DF[Device][R])
  del pd
  return DF
def GetDeviceData(DF, DT):
  return DF[DT]
def InvestigateDF(DF):
    Count = DF.isnull().count()
    Sum = DF.isnull().sum()
    Percentage = (Sum/Count*100)
    DataTypes = [str(DF[x].dtype) for x in DF.columns]
    return(np.transpose(pd.DataFrame({'Total':Sum, 'Precent':Percentage, 'Types':DataTypes})))
def CreateDirectory(P, Name):
    import os
    path = os.path.join(P, Name)
    try:
        a = os.mkdir(path)
        print ("Directory "+path+" created.")
        del a
    except:
        print ("Directory "+path+" already exists.")
    del os
    return path+"/"
def NeedThisModel(ModelDetails, Type):
    # NeedThis Model requires Location, ModelName in a dictionary object. Return True if Model doesn't exists or False if exists.
    # Second parameter is ModelType i.e. TF = TensorFlow or SK = SKLearn
    import os
    ModelPath = ModelDetails["Location"]+"/"+ModelDetails["ModelName"]
    TFLite = os.path.isfile(ModelPath+".tfl")
    SKLearn = os.path.isfile(ModelPath+".skl")
    TFMicro = os.path.isfile(ModelPath+".h")
    TFModel = os.path.isdir(ModelPath)
    if (Type=="TF" and TFModel):
        Exists = True
    elif(Type=="TFM" and TFMicro):
        Exists = True
    elif(Type=="TFL" and TFLite):
        Exists = True
    elif (Type=="SK" and SKLearn):
        Exists = True
    else:
        Exists = False
    if (Exists):
        return False
    else:
        return True
#Visualize Device Data
def VisualizeDeviceData(DF, DPs, Title="Train", Location=""):
  import plotly.express as px
  import plotly.graph_objects as go
  #import plotly.io.write_image
  fig = go.Figure()
  for DP in DPs:
    fig.add_trace(go.Scatter(x=DF["datetime"], y=DF[DP], mode='lines', name=DP))
  Title = Title+" "+"-".join(DPs)
  fig.update_layout(
  xaxis_title="Timeline",
  yaxis_title=Title)
  #fig.show()
  fig.write_html(Location+Title+".html")
  fig.data = []
def DPSubsets(DPs, MaxPoints=0, MinPoints=0):
    if (len(DPs)==1):
        #print ("List has only 1 DataPoint, No further subsets possible")
        return DPs
    from itertools import combinations
    from time import time
    ST = time()
    SubSet=[]
    if (MaxPoints==0):
        MaxPoints = len(DPs)
    for DP in range(MinPoints, MaxPoints+1):
        for ss in combinations(DPs, DP):
            if (len(ss)>0):
                SubSet.append(ss)
    del time
    del combinations
    return SubSet
def DataReducer(DF, DPs, Type):
    if (len(DPs)<=1):
        print ("Atleast two DataPoints should be provided for reduction")
        return False
    from time import time
    import numpy as np
    import pandas as pd
    try:
        from scipy import stats
    except:
        print ("Error, please install scipy version latest than 1.7.0")
        return False
    ST = time()
    t=[]
    if (Type=="Average"):
        for row in DF[DPs].to_numpy():
            row = np.nan_to_num(row)
            t.append(np.average(row))
    elif (Type=="SD"):
        for row in DF[DPs].to_numpy():
            row = np.nan_to_num(row)
            t.append(np.std(row))
    elif (Type=="Skew"):
        for row in DF[DPs].to_numpy():
            row = np.nan_to_num(row)
            t.append(stats.skew(row))
    elif (Type=="Kurtosis"):
        for row in DF[DPs].to_numpy():
            row = np.nan_to_num(row)
            t.append(stats.kurtosis(row))
    elif (Type=="MAD"):
        for row in DF[DPs].to_numpy():
            row = np.nan_to_num(row)
            t.append(stats.median_abs_deviation(row))
    else:
        print ("Please choose correct reduction technique: Average, SD, Skew, Kurtosis, MAD")
    t = pd.DataFrame(t,columns=['value'])
    ET = time()
    ProcessingTime = (ET-ST)*1000
    print ("Processing Time for "+Type+" Reduction of dataframe of shape: ", DF.shape, " = ", ProcessingTime)
    del np
    del stats
    del pd
    del time
    return t
def DataScaler(DF, DPs, Type="Standard"):
    from time import time
    import pandas as pd
    ST = time()
    DF[DPs] = DF[DPs].fillna(0)
    if (Type=="Standard"):
        from sklearn.preprocessing import StandardScaler
        R = pd.DataFrame(StandardScaler().fit_transform(pd.DataFrame(DF[DPs])),columns=[DPs])
        del StandardScaler
    elif(Type=="MinMax"):
        from sklearn.preprocessing import MinMaxScaler
        R = pd.DataFrame(MinMaxScaler().fit_transform(pd.DataFrame(DF[DPs])),columns=[DPs])
        del MinMaxScaler
    ET = time()
    ProcessingTime = (ET-ST)*1000
    print ("Processing Time for "+Type+" Scaling of dataframe of shape: ", DF.shape, " = ", ProcessingTime)
    del time, pd
    return R
def DataCleanerNormalizer(DF, DateColumn, DataMapping='auto', LabelColumn=False):
    # This function will clean DataFrame from null values, and auto/manual map strings with numbers
    import pandas as pd
    from time import time
    ST = time()
    # Getting all columns in the received DataFrame and convert into list to make it iterable
    Readings = DF.columns
    Readings = Readings.to_list()
    Readings.remove(DateColumn)
    # If lable column was configured, it will be removed from the process. 
    if (LabelColumn!=False):
        Readings.remove(LabelColumn)
    # Get all columns' datatypes to device either clean or normalize
    DataTypes = DF[Readings].dtypes
    # This Dictionary object will hold all mappings' information for later use.
    N = {}
    for i in range(len(Readings)):
        a = Readings[i]
        if (DataTypes[i] == object):
            if (DataMapping=='auto'):
                # This is normal dataset and has not been normalized yet
                U = DF[a].unique()
                ids = [x for x in range(len(U))]
                dic = dict(zip(U, ids))
                N[a]=dic
                DF[a] = DF[a].map(dic)
            else:
                # DataMapping has the mapping information, possible normal dataset has been normalized
                # print (a, DataMapping[a])
                DF[a] = DF[a].map(DataMapping[a])
                N[a] = DataMapping[a]
        else:
            # Convert all columns to numeric format
            DF[a] = pd.to_numeric(DF[a])
            # fill 0 where null values in DataFrame
            DF[a].fillna(0)
    # Convert Data/Time Column with datatime format
    DF[DateColumn] = pd.to_datetime(DF[DateColumn], errors="ignore")
    ET = time()
    ProcessingTime = (ET-ST)*1000
    print ("Processing Time for cleaning and normalization of dataframe of shape: ", DF.shape, " = ", ProcessingTime)
    if (N):
      pass
    else:
      print ("There is no columns to normalize, make sure you are not running this function on pre-normalized dataset.")
    del time
    del pd
    # Function will return Cleaned and Normalized DataFrame, DataPoints, Dictionary Object for Mapping (for reversing the Normalization), 
    # print ()
    return DF, Readings, N
def MergeDeviceData(DF, Devices, DPs):
  import pandas as pd
  M = pd.DataFrame()
  if (DF[Devices[0]].shape[0] >= DF[Devices[1]].shape[0]):
    DF[Devices[0]] = DF[Devices[0]][:DF[Devices[1]].shape[0]]
  elif (DF[Devices[0]].shape[0] < DF[Devices[1]].shape[0]):
    DF[Devices[1]] = DF[Devices[1]][:DF[Devices[0]].shape[0]]
#   print  (DF[Devices[0]].shape[0], DF[Devices[1]].shape[0])
  M["datetime"] = DF[Devices[0]]["datetime"]
  DFName = ""
  for i in range (len(Devices)):
    DFName = DFName+Devices[i]+"_"
    for DP in DPs[i]:
      M[Devices[i]+"_"+DP] = DF[Devices[i]][DP]
      DFName = DFName+DP+"-"
    DFName = DFName+"__"
  DFName = DFName.rstrip("_-")
  del pd
  return M, DFName
  #Double underscore to sperate Devices, single underscore to seperate devices and sensors and dash to seperate sensors